const priceBlocks = document.querySelectorAll('.price-block');
if (priceBlocks.length > 0) {
	const priceRequest = new XMLHttpRequest();
	priceRequest.open('GET', '/assets/price.json');
	priceRequest.send();
	for ( let i = 0; i < priceBlocks.length; i++ ) {
		const priceBlockCurrencies = document.createElement('div');
		priceBlockCurrencies.classList.add('price-block__currencies');
		const priceSelector = document.createElement('ul');
		priceSelector.classList.add('price-block__currencies-list');
		const currencySpan = document.createElement('span');
		currencySpan.innerHTML = 'Select currency: ';
		priceBlockCurrencies.appendChild(currencySpan);
		priceBlockCurrencies.appendChild(priceSelector);
		priceBlocks[i].appendChild(priceBlockCurrencies);
	}
	priceRequest.addEventListener('load', function () {
		if (priceRequest.status === 200) {
			const prices = JSON.parse(priceRequest.responseText);
			for ( const item in prices ) {
				for (const currency in prices[item].currencies ) {
					for ( let i = 0; i < priceBlocks.length; i++ ) {
						if ( priceBlocks[i].dataset.pricename === item ) {
							const priceSelector = priceBlocks[i].querySelector('.price-block__currencies-list');
							const priceSelectorItem = document.createElement('li');
							const currencyCode = prices[item].currencies[currency].code;
							const currencyName = prices[item].currencies[currency].name;
							const currencySymbol = prices[item].currencies[currency].symbol;
							priceSelectorItem.dataset.currency = currency;
							priceSelectorItem.classList.add('price-block__currency-title');
							priceSelectorItem.innerHTML = currencySymbol + ' <span>' + currencyName + '</span> (' + currencyCode + ')';
							priceSelector.appendChild(priceSelectorItem);
							const priceTable = document.createElement('table');
							priceTable.dataset.currency = currency;
							priceTable.classList.add('price-block__currency-table');
							priceTable.appendChild( document.createElement('tbody') );
							const priceFor = prices[item].pricefor;
							const pricesColsCount = prices[item].currencies[currency].prices.length;
							const priceTableBody = priceTable.querySelector('tbody');
							const priceTableTitle = document.createElement('tr');
							for ( let t = 0; t < 3; t++ ) {
								const td = document.createElement('td');
								if ( t !== 2 ) {
									priceTableTitle.appendChild(td);
								}else {
									td.setAttribute('colspan', pricesColsCount);
									td.innerHTML = '���� ��� ���������';
									priceTableTitle.appendChild(td);
								}
							}
							priceTableBody.appendChild(priceTableTitle);
							const priceTableHeader = document.createElement('tr');
							for ( let t = 0; t < pricesColsCount + 1; t++) {
								const td = document.createElement('td');
								if ( t === 0 ) {
									td.innerHTML = 'Length, cm (inch)';
									priceTableHeader.appendChild(td);
								}else if ( t === 1 ) {
									td.innerHTML = currencyCode + ', price for ' + priceFor;
									priceTableHeader.appendChild(td);
								}else {
									td.innerHTML = currencyCode + ', price for ' + priceFor + '<br><strong>discount ' + prices[item].discounts[t - 2] + '%</strong>';
									priceTableHeader.appendChild(td);
								}
							}
							priceTableBody.appendChild(priceTableHeader);
							prices[item].lengths.forEach( function ( length, index ) {
								const tr = document.createElement('tr');
								const td = document.createElement('td');
								td.innerText = length;
								tr.appendChild( td );
								prices[item].currencies[currency].prices.forEach( function ( itemPrice ) {
									const tdPrice = document.createElement('td');
									tdPrice.innerText = itemPrice[index] + ' ' + currencySymbol;
									tr.appendChild( tdPrice );
								} );
								priceTableBody.appendChild( tr );
							});
							priceBlocks[i].appendChild(priceTable);
						}
					}
				}
			}


			const priceSwitchers = document.querySelectorAll( '.price-block__currency-title' );
			const priceSwitchersFirst = document.querySelectorAll( '.price-block__currency-title:first-of-type' );
			for ( let i = 0; i < priceSwitchersFirst.length; i++ ) {
				priceSwitchersFirst[i].classList.add('price-block__currency-title_current');
			}

			for ( let i = 0; i < priceSwitchers.length; i++ ) {
				priceSwitchers[i].addEventListener( 'click', currencySwitch );
			}
		}
	});
}