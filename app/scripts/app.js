import $ from 'jquery';

const modalImage = document.querySelectorAll( 'a.modal' );

for ( let i = 0; i < modalImage.length; i++ ) {
	const item = modalImage[i];
	if (item.childElementCount === 1 && item.children[0].tagName === 'IMG' ){
		item.classList.add('modal-image');
	}
}

$('.faq-item__switch').click( function () {
	$('.faq-item__body').not($(this).next()).slideUp();
	$(this).next().slideToggle('.2');
} );

$(document).ready( function () {
	$('a[href^="#"]').click(function () {
		const elementClick = $(this).attr('href');
		const destination = $(elementClick).offset().top;
		$('body').animate( {scrollTop: destination}, 400 );
	});
});

function currencySwitch() {
	const priceTables = this.parentElement.parentElement.parentElement.querySelectorAll( '.price-block__currency-table' );
	const priceSwitchersCurrentBlock = this.parentElement.querySelectorAll( '.price-block__currency-title' );
	for ( let j = 0; j < priceTables.length; j++ ) {
		priceTables[j].style.display = 'none';
		priceSwitchersCurrentBlock[j].classList.remove('price-block__currency-title_current');
		if ( this.dataset.currency === priceTables[j].dataset.currency ) {
			priceTables[j].style.display = 'table';
		}
	}
	this.classList.add('price-block__currency-title_current');
}

const priceSwitchers = document.querySelectorAll( '.price-block__currency-title' );
const priceSwitchersFirst = document.querySelectorAll( '.price-block__currency-title:first-of-type' );
for ( let i = 0; i < priceSwitchersFirst.length; i++ ) {
	priceSwitchersFirst[i].classList.add('price-block__currency-title_current');
}

for ( let i = 0; i < priceSwitchers.length; i++ ) {
	priceSwitchers[i].addEventListener( 'click', currencySwitch );
}
